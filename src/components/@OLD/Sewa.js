import React from 'react'

const Sewa = () => {
  return (
    <div>
      <section className="sewa-section">
        <div className="sewa-container">
          <h1>Sewa Mobil di Jawa Timur Sekarang</h1>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Amet odio
            blanditiis voluptatem libero sit quam nisi deserunt quas,
            exercitationem, rem porro quia atque aspernatur cum, ipsum nemo
            nihil vel sed.
          </p>
          <button type="button" className="btn btn-success">
            Mulai Sewa Mobil
          </button>
        </div>
      </section>
    </div>
  );
}

export default Sewa